package rmu.titlisscraper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import rmu.titlisscraper.model.Peak;

/**
 *
 * @author regis
 */
public class HtmlReader {

    private List<Peak> peakList;

    public HtmlReader(String webPage) {
        peakList = new ArrayList<>();
        try {
            Document doc = Jsoup.connect(webPage).get();
            String html = doc.html();
            Element div = doc.select("div.pimcore_area_titlisWetterInformation").first();
            Element table = doc.select("table.table-weather").first();
            Elements trs = table.getElementsByTag("tr");

            trs.remove(0);
            for (Element tr : trs) {
                Elements tds = tr.getElementsByTag("td");
                Peak peak = new Peak(tds.get(0).text(), tds.get(1).text().replace("°C", ""), tds.get(2).text().replace(" cm", ""), tds.get(3).text().replace(" cm", ""));
                peakList.add(peak);
            }

        } catch (IOException exception) {
            System.out.println(exception.getMessage());
        }
    }

    public List<Peak> getPeakList() {
        return peakList;
    }

}
