package rmu.titlisscraper.model;

/**
 *
 * @author regis
 */
public class Peak {
    private String name;

    public Peak(String name, String temperature, String snowfall, String newSnow24h) {
        this.name = name;
        this.temperature = temperature;
        this.snowfall = snowfall;
        this.newSnow24h = newSnow24h;
    }
    private String temperature;
    private String snowfall;
    private String newSnow24h;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getSnowfall() {
        return snowfall;
    }

    public void setSnowfall(String snowfall) {
        this.snowfall = snowfall;
    }

    public String getNewSnow24h() {
        return newSnow24h;
    }

    public void setNewSnow24h(String newSnow24h) {
        this.newSnow24h = newSnow24h;
    }

    public Peak() {
    }
    
    
}
