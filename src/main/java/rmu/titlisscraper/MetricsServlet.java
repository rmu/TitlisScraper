package rmu.titlisscraper;

import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.Gauge;
import io.prometheus.client.exporter.common.TextFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import rmu.titlisscraper.model.Peak;

public class MetricsServlet extends HttpServlet {

    private CollectorRegistry registry;
    static final int PORT = 1234;
    static final String labelNames = "Peak";

    static final Gauge temperatures = Gauge.build("engelberg_temperatures_in_c", "Temperatures in Celcius").labelNames(labelNames).register();
    static final Gauge snowFall = Gauge.build("engelberg_snowfall_in_cm", "Depth of snow in cm").labelNames(labelNames).register();
    static final Gauge newSnow = Gauge.build("engelberg_new_snow_24_h", "New snow in cm in the last 24 h").labelNames(labelNames).register();

    /**
     * Construct a MetricsServlet for the default registry.
     */
    public MetricsServlet() {
        this(CollectorRegistry.defaultRegistry);
    }

    /**
     * Construct a MetricsServlet for the given registry.
     */
    public MetricsServlet(CollectorRegistry registry) {
        this.registry = registry;
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {

        HtmlReader reader = new HtmlReader("https://www.titlis.ch/en/live/weather");
        List<Peak> peakList = reader.getPeakList();

        for (Peak peak : peakList) {
            temperatures.labels(peak.getName()).set(Double.parseDouble(peak.getTemperature()));
            snowFall.labels(peak.getName()).set(Double.parseDouble(peak.getSnowfall()));
            newSnow.labels(peak.getName()).set(Double.parseDouble(peak.getNewSnow24h()));
        }
        resp.setStatus(HttpServletResponse.SC_OK);
        resp.setContentType(TextFormat.CONTENT_TYPE_004);

        Writer writer = resp.getWriter();
        try {
            TextFormat.write004(writer, registry.filteredMetricFamilySamples(parse(req)));
            writer.flush();
        } finally {
            writer.close();
        }
    }

    private Set<String> parse(HttpServletRequest req) {
        String[] includedParam = req.getParameterValues("name[]");
        if (includedParam == null) {
            return Collections.emptySet();
        } else {
            return new HashSet<String>(Arrays.asList(includedParam));
        }
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }

    public static void main(String[] args) throws Exception {
        Server server = new Server(PORT);
        ServletContextHandler context = new ServletContextHandler();
        context.setContextPath("/");
        server.setHandler(context);
        // Expose Promtheus metrics.
        context.addServlet(new ServletHolder(new MetricsServlet()), "/metrics");
        // Add metrics about CPU, JVM memory etc.
        //DefaultExports.initialize();

        // Start the webserver.
        server.start();
        server.join();
    }
}
