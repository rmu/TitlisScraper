package rmu.titlisscraper;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author regis
 */
public class HtmlReaderTest {
    
    public HtmlReaderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {        
        HtmlReader reader = new HtmlReader("https://www.titlis.ch/en/live/weather");
        assertTrue(reader.getPeakList().size() == 5);        
    }
    
}
